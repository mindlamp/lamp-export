import { config } from "dotenv";

config();
import Debug from "debug";
import async from "async";
import { Command, Option } from "@commander-js/extra-typings";
import Nano, {
  DocumentListParams,
  DocumentResponseRow,
  DocumentScope,
  MangoQuery,
} from "nano";
import nano from "nano";
import * as http from "http";
import { Readable } from "node:stream";
import { pipeline } from "node:stream/promises";
import * as fs from "node:fs/promises";
import path from "node:path";
const debug = Debug("lamp-export");

debug("Started");

const CONCURRENCY = 8;
const BLOCK_SIZE = 100;

const program = new Command()
  .addOption(
    new Option("-s, --source <url>", "").env("SOURCE").makeOptionMandatory(),
  )
  .addOption(
    new Option("--source-password <source_password>", "Source password").env(
      "SOURCE_PASSWORD",
    ),
  )
  .addOption(
    new Option("--source-username <source_username>", "Source username").env(
      "SOURCE_USERNAME",
    ),
  )
  .addOption(
    new Option("--start-date <start_date>", "Start date")
      .env("START_DATE")
      .default("2023-08-01T00:00:00Z", "2023-10-01"),
  )
  .addOption(
    new Option("--end-date <end_date>", "End date")
      .env("END_DATE")
      .default("2024-01-01T00:00:00Z", "2024-01-01"),
  )
  .addOption(
    new Option("--output <output_dir>", "output directory").env("OUTPUT_DIR"),
  );

// The main entry point for the tool
program.action(async (config) => {
  // Tweak the default CouchDB client to limit the maximum number of connections
  // and use KeepAlive
  const agent = new http.Agent({
    keepAlive: true,
    maxSockets: 50,
  });
  // Set up database scope
  const client = Nano({
    url: config.source,
    requestDefaults: {
      auth: {
        username: config.sourceUsername!,
        password: config.sourcePassword!,
      },
      agent: agent,
    },
  });

  const startDate = new Date(config.startDate);
  const endDate = new Date(config.endDate);

  // start doing work
  await getParticipants(client, config.output!, startDate, endDate);
});

program.parse();

/**
 * CouchDB document types
 */

// Timestamped docs have a `timestamp`
interface Timestamped {
  timestamp: number; // EMACScript Epoch
}

interface Owned {
  "#parent": string; // ID of entity that owns this document
}

// Basic participant model
interface Participant extends Nano.Document, Timestamped, Owned {}

// Basic SensorEvent model
interface SensorEvent extends Nano.Document, Timestamped, Owned {
  data: object;
}

/**
 * Fetches all the participants from the `participant` collection of the provided database scope
 * @param client the database scope to use
 * @param output the path to output the results to
 * @param startDate
 * @param endDate
 */
async function getParticipants(
  client: nano.ServerScope,
  output: string,
  startDate: Date,
  endDate: Date,
) {
  // Defines the date range to export

  debug("Getting participants");

  // A pipeline to list all the participants and filtering out any design documents and any participant added as part of the open testing
  const participants = await Readable.from(
    streamParticipants<Participant>(
      client.db.use<Participant>("participant"),
      100,
    ),
    { objectMode: true },
  )
    .filter(
      (doc: DocumentResponseRow<Participant>) => !doc.id.startsWith("_design/"),
    )
    .filter(
      (doc: DocumentResponseRow<Participant>) =>
        doc.doc?.["#parent"] !== "dolor ut",
    ) // Filter garbage participants
    .map((doc) => doc.doc)
    .toArray();

  debug("Got %d participants", participants.length);

  const SiteCode = "ME";

  // Defines an individual participant / day pair and where to export it to
  type Task = {
    participantId: string;
    start: Date;
    end: Date;
    filename: string;
  };

  const outDir = path.normalize(output);

  // Create the main work queue responsible for loading and storing a task. It's an `async` queue with CONCURRENCY workers
  const workQueue = async.queue(async function (task: Task) {
    // Create a local debug context for just this task
    const participantDebug = Debug(
      `lamp-export:${task.participantId}:${task.start
        .toISOString()
        .substring(0, 10)}`,
    );
    participantDebug("Starting");

    // Generate the output path
    const output = path.join(
      outDir,
      `${task.participantId}_Prescient${SiteCode}_sensor_${task.start
        .toISOString()
        .substring(0, 10)
        .replace(/-/g, "_")}.json`,
    );

    // Generate the path we'll be uploading to before moving to the final location
    const tmpPath = path.join(outDir, path.basename(output) + ".tmp");
    let eventCount = 0;

    try {
      // If a final file for this task exists skip it.
      await fs.access(output, fs.constants.F_OK);
      participantDebug("Already done");
    } catch (err) {
      // Output file doesn't exist so do work
      participantDebug("Does not already exist");

      const outputFd = await fs.open(tmpPath, "w");
      const outputStream = outputFd.createWriteStream({
        autoClose: true,
        encoding: "utf8",
      });
      participantDebug("Starting");
      try {
        // Kick off the pipeline to process this task
        await pipeline(
          streamSensorEvents(
            client,
            task.participantId,
            task.start,
            task.end,
            BLOCK_SIZE,
          ), // Stream all the results
          async function* (source) {
            // Since we're streaming the data out we can't use stringify on the collection as we'd get a
            // sequence of JSON arrays so we need to do a little more work

            // Write out the result stream
            yield "[\n"; // Start a JSON array

            // Wait for batches of events to arrive
            for await (const events of source) {
              if (events.length !== 0) {
                // Write out a non-empty batch of events
                eventCount += events.length;
                // write out each event as a compact JSON object (single line no pretty-printing)
                const out = events.map((event) => {
                  return JSON.stringify(event, ["data", "sensor", "timestamp"]);
                });
                yield out.join(",\n"); // add the array separator and start a new line.
              } else {
                yield "\n";
              }
            }
            yield "]"; // Close the array
          },
          outputStream,
        );
      } finally {
        // We've (probably) written a complete file so move it to from the temp to final location
        await fs.rename(tmpPath, output);
        // Log we're done
        participantDebug(
          "Processed %d events for %s",
          eventCount,
          new Date(task.start).toISOString(),
        );
      }
    }
  }, CONCURRENCY);

  // Create the output directory if it didn't exist
  await fs.mkdir(outDir, { recursive: true });

  // Make the first day's start and end time stamps
  let dayStart = new Date(startDate);
  let dayEnd = new Date(startDate);
  dayEnd = new Date(dayEnd.setDate(dayStart.getDate() + 1));

  debug(
    "Fetching from %s to %s",
    startDate.toISOString(),
    endDate.toISOString(),
  );

  // The main task generation loop
  while (dayStart <= endDate) {
    debug("Processing from %s", dayStart.toISOString().substring(0, 10));
    // For each participant we've found create a task to export this day's events
    for (const participant of participants) {
      // Load into the queue
      workQueue.push(
        {
          participantId: participant._id,
          start: dayStart,
          end: dayEnd,
          filename: `${participant._id}_Prescient${SiteCode}_sensor_${dayStart
            .toISOString()
            .substring(0, 10)
            .replace(/-/g, "_")}.json`,
        },
        /*eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }]*/
        (err, _result) => {
          if (err) {
            console.error("Encountered error ", err);
          }
        },
      );
    }
    // Cycle dayStart to the next day
    dayStart = new Date(dayEnd);
    const endTimestamp = dayEnd.setDate(dayStart.getDate() + 1);
    dayEnd = new Date(endTimestamp);

    /**
     * TODO: having drain inside the loop means we process each day completely before moving on.
     * This limits the number of jobs in the queue but costs us some time. I didn't intend for this
     * but it's what we've been using so I'll commit this for now.
     */

    // Wait for all the jobs to finish
    await workQueue.drain();
  }
}

/**
 * An AsyncGenerator to load all the sensor events for a specific participant between two timestamps
 * @param client the ServerScope to target
 * @param participantId the ID of the participant to fetch
 * @param start the Date to start from (inclusive)
 * @param end the Date to end at (exclusive)
 * @param limit the maximum number of entries per individual fetch (default: 100)
 */
async function* streamSensorEvents(
  client: nano.ServerScope,
  participantId: string,
  start: Date,
  end: Date,
  limit: number = 100,
) {
  const debugging = Debug(`lamp-tools:sensor-events:${participantId}`);
  const sensor_events = client.db.use<SensorEvent>("sensor_event");

  let done: boolean = false;

  // The Mango query we'll be using
  const query: MangoQuery = {
    selector: {
      "#parent": participantId,
      timestamp: {
        $and: [
          {
            $gte: start.getTime(),
            $lt: end.getTime(),
          },
        ],
      },
    },
    fields: ["data", "sensor", "timestamp"],
    limit,
    // these settings are for performance in a cluster environment
    update: false,
    stable: true,
  };

  while (!done) {
    // Fetch a block of data
    debugging("Querying");
    const result = await sensor_events.find(query);
    debugging("Done query");

    if (result.warning) {
      // We got an error. Stop. (We should probably have a retry here)
      debugging("Got warning %s", result.warning);
      done = true;
    } else {
      debugging(
        "Got %d events bookmark: %s ",
        result.docs.length,
        result.bookmark,
      );
      // If there's a bookmark set it in the query so we fetch the next block of events next time
      if (result.bookmark && result.bookmark !== "nil") {
        query.bookmark = result.bookmark;
        done = result.docs.length < limit; // If we got less than a full block's worth we hit the end of the sequence
      } else {
        // Included for completeness. We shouldn't ever get a valid response without a bookmark entry, but it's
        // a logical option and the linter complains otherwise.
        done = true;
      }
      // Pass this block down the pipeline and keep going
      yield result.docs;
    }
  }
}

/**
 * An AsyncGenerator to load all the participants. It pages through the participants in blocks of up to `limit` entries.
 * See: https://docs.couchdb.org/en/stable/ddocs/views/pagination.html#paging-alternate-method for context
 * @param db the SensorScope to use
 * @param limit the maximum number of entries per-request
 */
async function* streamParticipants<T>(
  db: DocumentScope<T>,
  limit: number = 100,
) {
  // Define a type for a page of data. An array of documents and an optional next document if it's not the last page
  type ViewResult = [
    data: Nano.DocumentResponseRow<T>[],
    next: Nano.DocumentResponseRow<T> | undefined,
  ];

  /**
   * Helper function for loading a page of data and the pointer to the next page if it exists
   * @param params CouchDB's parameters for a view query
   * @return a tuple of and array of at most limit-1 entries and the limit-th entry or undefined if there was less than
   * that many entries returned.
   */
  async function fetch(params: DocumentListParams): Promise<ViewResult> {
    const result = await db.list(params);
    // We got the limit's worth of data so we need to do at least one more query
    if (result.rows.length === limit) {
      return [result.rows.slice(0, -1), result.rows.slice(-1)[0]];
    } else {
      // Hit the end of the view so return what data we got and `undefined` for the pointer
      return [result.rows, undefined];
    }
  }

  // Load the first batch of participants
  let [docs, next] = await fetch({
    include_docs: true, // There wasn't a performance hit for just loading docs here to keep it simple
    limit,
  });
  // Stream out each participant in the first batch
  for (const doc of docs) {
    yield doc;
  }

  // Now we've initialised the query we can continue to page through the entries
  while (next) {
    try {
      [docs, next] = await fetch({
        include_docs: true,
        start_key_doc_id: next.id,
        start_key: next.key,
        limit: limit,
      });
      for (const doc of docs) {
        yield doc;
      }
    } catch (e) {
      console.error(e);
    }
  }
}
