LAMP-Export
===========

A simple as possible tool to export data from a CouchDB instance backing a
[LAMP Server](https://github.com/BIDMCDigitalPsychiatry/LAMP-server) instance.

Running
-------

To run check out the code and use:

```bash
npm run build
# then 
node --enable-source-maps dist/index.js --source http://couchdb.example.org:5984 --source-username $USERNAME --source-password $PASSWORD --output /var/lib/prescient/data/
```
Parameters can also be set as environment variables in the shell or with a `.env` file. See `.env.example`.




